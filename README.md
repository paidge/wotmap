# Wotmap
Exploration de la Toile de Confiance de Duniter utilisant la librairie javascript [SigmaJS en version 1.2](http://sigmajs.org/) pour le front et la bibliothèque Python [NeworkX](https://networkx.org) pour interroger un noeud duniter local.

## Tester en ligne
- [https://wotmap.duniter.org](https://wotmap.duniter.org)


![Capture](img/capture.jpg)
## Fonctionnalités

* Importation des données
  * 1 script Python est fourni pour récupérer les données directement depuis la base de données de votre noeud Duniter local ainsi que les photos des membres depuis une instance duniter4j
  * Un fichier JSON avec les noeuds et les liens est fourni
  * Un fichier JSON avec les stats est fourni
* Affichage de la toile de confiance sous forme de graphe
  * Les noeuds sont les membres certifiés
  * La taille de chaque noeud est paramétrable en fonction du nombre de certifications reçues, émises, total ou en fonction du nombre de voisins
  * Les liens entre les noeuds représentent les certifications entre chaque membres
* Fonctionnalités
  * Utilisation de la souris pour se déplacer et zoomer sur la wot
  * Cliquez sur un noeud pour ne laisser apparaître que ses voisins directs. Cliquez à nouveau sur le noeud en question pour revenir à l'état initial.
  * Affichage d'un pop-up avec des informations sur le noeud sélectionné
  * Détection de communautés (Label Propagation Algorithm)
  * Double-cliquez sur un noeud pour ne laisser apparaître que sa communauté. Double-cliquez à nouveau sur un noeud pour réafficher l'ensemble de la toile.
  * Faites un clic droit sur un noeud pour masquer sa communauté.
  * Utilisez l'historique de votre navigateur pour revenir sur les noeuds du graphe que vous avez visités
  * Fonction de recherche avec auto-complétion
  * Filtrage des noeuds en fonction du champ référent et/ou en fonction du nombre minimum de voisins (degree)
  * Affichage ou non des avatars cesium+
  * Affichage ou non des noms des membres
  * Affichage ou non des liens pour économiser des ressources machines
  * Possibilité d'afficher le graphe sous forme d'un cercle (StrongGravityMode)

## Installation
### I. Installer Python et les paquets nécessaires

	sudo apt-get update && sudo apt-get install python3 python3-pip
	sudo pip3 install plyvel networkx numpy fa2rs

Si, au lancement du script wot_json.py, vous obtenez une erreur liée à GLIBC c'est que vous devez compiler le paquet pour votre machine et cela nécessire Rust.

	# On installe Rust
	sudo curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
	sudo rustup install nightly
	sudo rustup default nightly
	# On récupère les sources
	mkdir sources_fa2rs && cd sources_fa2rs
	git clone https://framagit.org/ZettaScript/forceatlas2-rs forceatlas2
	git clone https://framagit.org/ZettaScript/fa2rs-py
	cd fa2rs-py
	sh build_wheel.sh
	sudo pip3 install --user --force-reinstall target/wheel/dist/fa2rs-0.1.0-py3-none-any.whl

	
### II. Récupérer les sources du projet
Rendez-vous dans le dossier HOME de votre utilisateur duniter (par ex : /var/lib/duniter/) et récupérez les sources :

	cd /var/lib/duniter/ && sudo -u duniter git clone https://git.duniter.org/paidge/wotmap.git

### III. Automatisation de la mise à jour des données (CRON)
* Créez le dossier qui accueillera les logs (l'utilisateur duniter doit avoir les droits d'écriture) :

		sudo mkdir /var/log/wotmap && chown -R duniter:duniter /var/log/wotmap

* Créez le fichier ***/etc/cron.d/wotmap*** sur votre serveur pour automatiser le lancement du script :

		30 3 * * 0-5 duniter python3 /var/lib/duniter/wotmap/script/wot_json.py --ns --fa2rs >> /var/log/wotmap/wotmap.log 2>&1
		30 3 * * 6 duniter python3 /var/lib/duniter/wotmap/script/wot_json.py -d --ns --fa2rs >> /var/log/wotmap/wotmap.log 2>&1

	
### IV. Paramétrage d'Apache
* Créez un virtualhost pour votre nom de domaine

		sudo nano /etc/apache2/sites-available/mondomaine.fr.conf

* Pour y placer votre configuration :

		<VirtualHost *:443>
			ServerName mondomaine.fr
			DocumentRoot /var/lib/duniter/wotmap/
			<Directory /var/lib/duniter/wotmap/>
					Options Indexes FollowSymLinks MultiViews
					AllowOverride all
					Require all granted
					Header set Access-Control-Allow-Origin '*'
			</Directory>
			SSLCertificateFile /etc/letsencrypt/live/mondomaine.fr/fullchain.pem
			SSLCertificateKeyFile /etc/letsencrypt/live/mondomaine.fr/privkey.pem
		</VirtualHost>
	
* Puis activez-le :

		sudo a2ensite mondomaine.fr.conf
		sudo systemctl reload apache2

## Documentation

### Sortie de wot_json.py

**wot.json**

	{
		"nodes": [ // identities
			{
				"id": int,    // node ID, from 0
				"label": str, // identity UID
				"url": str,   // avatar URL
				"x": float,   // x position
				"y": float,   // y position
				"my_community": int, // community ID, from 0
				"size": int,  // node size (default: degree)
				"color": str,  // node color (format: CSS color; default: community)
				"attributes": {
					"pubkey": str,    // identity public key
					"member": bool,   // is identity member
					"degree": int,    // node degree
					"referent": bool, // is member referent
					"outDegree": int, // out degree
					"inDegree": int,  // in degree
					"written_time": int, // timestamp of first identity written in blockchain
					"blocks": int,       // number of blocks written by this public key
					"c_betweenness": float, // betweenness centrality
					"c_load": float,        // load centrality
					"c_eigenvector": float, // eigenvector centrality
					"c_degree": float,      // degree centrality
					"c_closeness": float,   // closenesscentrality
					"c_information": float, // information centrality
					"c_harmonic": float     // harmonic centrality
				}
			},
			...
		],
		"edges": [ // certifications
			{
				"id": str,             // edge ID, from 0
				"size": 1,             // line size
				"type": "curvedArrow", // line type
				"source": str,         // cert issuer
				"target": str,         // cert receiver
				"start_cert": int,     // cert start timestamp
				"end_cert": int        // cert end timestamp
			}
		]
	}

**stats.json**

	{
		"stats": {
			"median_time": int,   // latest block median time
			"total_edges": int,   // total number of edges
			"total_nodes": int,   // total number of nodes
			"total_members": int, // total number of members
			"total_communities": int, // total number of communities
			"crit_referent": int,     // min in&out degrees to be referent
			"total_referents": int,   // total number of referents
			"total_noreferents": int, // totel number of not referents
			"centrality_extrema": {   // centrality extrema [min, max]
				"c_betweenness": [float, float],
				"c_load": [float, float],
				"c_eigenvector": [float, float],
				"c_degree": [float, float],
				"c_closeness": [float, float],
				"c_information": [float, float],
				"c_harmonic": [float, float]
			}
		},
		"instance": {
			"software": "wotmap",
			"version": str,
			"working": bool,
			"maintainers": [str],
			"custom": {
				"currency": str, // currency code
				"block": int     // latest block number
			}
		}
	}
