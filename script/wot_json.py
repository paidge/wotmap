#!/usr/bin/env python3
"""
Copyright 2019-2020 Pascal Engélibert <tuxmain@zettascript.org>, Pierre-Jean Chancellier <paidge@normandie-libre.fr>
License GNU AGPL v3 (http://www.gnu.org/licenses/#AGPL)

Dependances: python3-plyvel, python3-networkx, numpy
Command for installing:
$ sudo pip3 install plyvel networkx numpy
"""

import sys, os, time, math, json, urllib.request, plyvel, subprocess, networkx, random

VERSION = "0.4.0"
STEPMAX = 5

def getargv(arg, default=""):
	if arg in sys.argv and len(sys.argv) > sys.argv.index(arg)+1:
		return sys.argv[sys.argv.index(arg)+1]
	else:
		return default

LOG_TRACE = 1
LOG_INFO = 2
LOG_WARN = 4
LOG_ERROR = 8
LOGMSG_TYPES = {LOG_INFO:"\033[96minfo\033[0m", LOG_TRACE:"\033[39mtrace\033[0m", LOG_WARN:"\033[93mwarn\033[0m", LOG_ERROR:"\033[91merror\033[0m"}
VERBOSITY = LOG_INFO | LOG_WARN | LOG_ERROR
NO_REVOKED = True # Do not display revoked identities
LAB_KN, LAB_XN, LAB_YN, LAB_ZN, LAB_T0, LAB_T1, LAB_T2, LAB_T3 = 18, 0.950470, 1, 1.088830, 0.137931034, 0.206896552, 0.12841855, 0.008856452

def lab_xyz(t):
	if t > LAB_T1: return t**3
	else: return LAB_T2*(t-LAB_T0)

def xyz_rgb(r):
	return max(0, min(255, round(255*(12.92*r if r<=0.00304 else 1.055*r**(1/2.4)-0.055))))

# l:[0;100] a:[-110;110] b:[-110;110]
# from https://github.com/gka/chroma.js
def lab2rgb(l, a, b):
	y = (l+16)/116
	x = y+a/500
	z = y-b/200
	y = LAB_YN*lab_xyz(y)
	x = LAB_XN*lab_xyz(x)
	z = LAB_ZN*lab_xyz(z)
	r = xyz_rgb(3.2404542*x - 1.5371385*y - 0.4985314*z);
	g = xyz_rgb(-0.9692660*x + 1.8760108*y + 0.0415560*z);
	b = xyz_rgb(0.0556434*x - 0.2040259*y + 1.0572252*z);
	return (r, g, b)

# t:[0;2pi]
def angle2square(t):
	if t >= 7*math.pi/4 or t <= math.pi/4:
		return (1, math.tan(t))
	elif t <= 3*math.pi/4:
		return (math.tan(math.pi/4-t), 1)
	elif t <= 5*math.pi/4:
		return (-1, math.tan(math.pi-t))
	else:
		return (math.tan(t-3*math.pi/2), -1)

# t:[0;1]
def lab_gradient(t, l=100):
	t *= 1.75*math.pi # from red to magenta
	(a, b) = angle2square(t)
	return lab2rgb(l, 110*a, 110*b)

def log(msg, msgtype=LOG_TRACE, end="\n", header=True):
	if msgtype & VERBOSITY:
		if header:
			print(time.strftime("%Y-%m-%d %H:%M:%S")+" ["+LOGMSG_TYPES[msgtype]+"] "+msg, end=end)
		else:
			print(msg, end=end)

class Node:
	def __init__(self, pubkey, member=False, uid="", written_on=None):
		self.pubkey = pubkey
		self.member = member
		self.uid = uid
		self.written_on = written_on
		self.nb = None
		self.certs_issued = []
		self.neighbors = []
		self.nbNeighbors = 0
		self.in_degree = 0
		self.out_degree = 0
		self.referent = 0
		self.blocks_written = 0

if __name__ == "__main__":
	if "--help" in sys.argv or "-h" in sys.argv:
		print("""This script extracts data from Duniter DB and generates output JSON files, with stats, positionned members (colored with communities) and certs on a graph.

Options:
 -i <dir>     Input duniter db (LevelDB)
 --ow <file>  Output JSON (wot)
 --os <file>  Output JSON (stats)
 -e <url>     URL of the ElasticSearch server
 -d           Download avatars
 -a <dir>     Avatars local path
 -A <dir>     Avatars public local path
 -m <int>     Avatars cache max age (s)
 -M <file>    Default avatar public local path
 -s           Do not stop & restart Duniter-ts
 --no-pos     Do not compute node coords
 -C <int>     Number of communities
 --ns         Disable strict mode (ignore incoherent data)
 --fa2rs      Use fa2rs implementation
 --csv <file> Export WoT graph to CSV (for Gephi)
 -v           Verbose

Default options:
 -i    ~/.config/duniter/duniter_default/data/leveldb
 --ow  ~/wotmap/data/wot.json
 --os  ~/wotmap/data/stats.json
 -e    https://g1.data.le-sou.org
 -a    ../img/photos
 -A    img/photos
 -m    604800  (7 days)
 -M    img/logo-g1.png
 -C    30
 --csv ~/wotmap/data/wot.csv""")
		exit()
	
	if "-v" in sys.argv:
		VERBOSITY |= LOG_TRACE
	
	# Initialize
	if not "-s" in sys.argv:
		log(subprocess.run(["duniter", "stop"], stdout=subprocess.PIPE).stdout.decode("utf-8"))
	
	outdata_wot = {"nodes":[], "edges":[]}
	outdata_stats = {"stats":{}, "instance":{"software": "wotmap", "version": VERSION, "working": True, "maintainers": [], "custom": {}}}
	
	# Read args
	dbpath = os.path.expanduser(getargv("-i", "~/.config/duniter/duniter_default/data/leveldb"))
	outpath_wot = os.path.expanduser(getargv("--ow", "~/wotmap/data/wot.json"))
	outpath_stats = os.path.expanduser(getargv("--os", "~/wotmap/data/stats.json"))
	url_es = getargv("-e", "https://g1.data.duniter.fr")
	download_avatars = "-d" in sys.argv
	avatars_path = os.path.expanduser(getargv("-a", os.path.dirname(os.path.abspath(__file__))+"/../img/photos"))
	avatars_url_path = getargv("-A", "img/photos")
	avatars_max_age = time.time() - int(getargv("-m", 604800))# 7 days
	avatars_default_url_path = getargv("-M", "img/logo-g1.png")
	gencoords = not "--no-pos"  in sys.argv
	strict = not "--ns" in sys.argv
	outpath_csv = os.path.expanduser(getargv("--csv", "~/wotmap/data/wot.csv"))
	verbose = "-v" in sys.argv
	
	log("Opening DBs...")
	iindex, cindex, bindex = 0, 0, 0
	while not (iindex and cindex):
		try:
			iindex = plyvel.DB(dbpath+"/level_iindex/")
			cindex = plyvel.DB(dbpath+"/level_cindex/")
			bindex = plyvel.DB(dbpath+"/level_bindex/")
		except plyvel._plyvel.IOError:
			time.sleep(0.5)
	
	# Get latest block
	latest_block = json.loads(bindex.iterator(include_key=False, reverse=True).__next__().decode())
	outdata_stats["instance"]["custom"]["currency"] = latest_block["currency"]
	outdata_stats["instance"]["custom"]["block"] = latest_block["number"]
	median_time = latest_block["medianTime"]
	outdata_stats["stats"]["median_time"] = median_time
	
	graph = networkx.Graph()
	
	log("Listing nodes...")
	tmp_nodes = []
	tmp_index = {}
	for pub, row in iindex:
		idty = json.loads(row.decode())[0]
		
		if NO_REVOKED and not idty["member"]:
			continue
		
		node = Node(idty["pub"], idty["member"], idty["uid"], idty["writtenOn"])
		
		# Index issued certs
		certs = json.loads(cindex.get(pub).decode())
		for cert in certs["issued"]:
			if not "expires_on" in cert:
				continue
			if cert["expires_on"] > median_time:
				node.certs_issued.append(cert)
		
		tmp_index[node.pubkey] = node
		tmp_nodes.append(node)
	
	log("Counting written blocks...")
	for block in bindex.iterator(include_key=False):
		block = json.loads(block.decode())
		if block["issuer"] in tmp_index:
			tmp_index[block["issuer"]].blocks_written += 1
	
	# Link nodes
	log("Linking nodes...")
	for node in tmp_nodes:
		for cert in node.certs_issued:
			try:
				receiver = tmp_index[cert["receiver"]]
			except KeyError:
				if strict:
					raise Exception("Strict mode enabled")
				else:
					continue
			
			if not node in receiver.neighbors:
				receiver.neighbors.append(node)
			if not receiver in node.neighbors:
				node.neighbors.append(receiver)
			
			receiver.in_degree += 1
			node.out_degree += 1
	
	log("Generating nodes...")
	index = {}
	nodes = []
	nb_nodes = 0
	nb_members = 0
	for node in tmp_nodes:
		node.nbNeighbors = len(node.neighbors) # TODO fix (choose neighbors or degree)
		if node.nbNeighbors == 0:
			continue
		
		node.nb = nb_nodes
		nb_nodes += 1
		if node.member:
			nb_members += 1
		index[node.pubkey] = node
		nodes.append(node)
		
		written_on = bindex.get(str(node.written_on).zfill(10).encode())
		if written_on != None:
			block = json.loads(written_on.decode())
			written_time = block["time"]
		else:
			written_time = None
		
		avatar_path = avatars_path+"/"+node.pubkey+".png"
		avatar_exists = os.path.isfile(avatar_path)
		if download_avatars:
			if not avatar_exists or os.path.getmtime(avatar_path) < avatars_max_age:
				try:
					log("Download avatar: " + node.pubkey, end=" ")
					response = urllib.request.urlopen(url_es+"/user/profile/"+node.pubkey+"/_image/avatar.png")
					f = open(avatar_path, "wb")
					f.write(response.read())
					f.close()
					log("OK", header=False)
					avatar_url = avatars_url_path+"/"+node.pubkey+".png"
				except urllib.error.HTTPError:
					avatar_url = avatars_default_url_path
					log("ERR", header=False)
			else:
				avatar_url = avatars_url_path+"/"+node.pubkey+".png"
		elif avatar_exists:
			avatar_url = avatars_url_path+"/"+node.pubkey+".png"
		else:
			avatar_url = avatars_default_url_path
		
		outdata_wot["nodes"].append({
				"id": str(node.nb),
				"label": node.uid,
				"url": avatar_url,
				"x": 0,
				"y": 0,
				"my_community": 0,
				"size": node.nbNeighbors,
				"color": "rgba(255,0,0,1)",
				"attributes": {
					"pubkey": node.pubkey,
					"member": node.member,
					"nbNeighbors": node.nbNeighbors,
					"referent": False,
					"outDegree": node.out_degree,
					"inDegree": node.in_degree,
					"written_time": written_time,
					"blocks": node.blocks_written,
					"c_betweenness": 0,
					"c_load": 0,
					"c_eigenvector": 0,
					"c_degree": 0,
					"c_closeness": 0,
					"c_information": 0,
					"c_harmonic": 0
				}
			})
	
	graph.add_nodes_from(range(0, nb_nodes))
	
	log("Generating edges...")
	nb_edges = 0
	for node in nodes:
		for cert in node.certs_issued:
			try:
				receiver = index[cert["receiver"]]
			except KeyError:
				if strict:
					raise Exception("Strict mode enabled")
				else:
					continue
			graph.add_edge(node.nb, receiver.nb)
			outdata_wot["edges"].append({
				"id": str(nb_edges),
				"size": 1,
				"type": "curvedArrow",
				"source": str(node.nb),
				"target": str(receiver.nb),
				"start_cert": cert["chainable_on"],
				"end_cert": cert["expires_on"]
			})
			nb_edges += 1
	
	iindex.close()
	outdata_stats["stats"]["total_edges"] = nb_edges
	outdata_stats["stats"]["total_nodes"] = nb_nodes
	outdata_stats["stats"]["total_members"] = nb_members
	
	if "--csv" in sys.argv:
		log("Writing CSV...")
		f = open(outpath_csv, "w")
		f.write("Source;Target\n")
		for edge in outdata_wot["edges"]:
			f.write("{};{}\n".format(edge["source"], edge["target"]))
		f.close()
	
	log("Generating communities...")
	communities = [i for i in networkx.community.asyn_fluidc(graph, int(getargv("-C", 30)))]
	nb_communities = len(communities)
	outdata_stats["stats"]["total_communities"] = nb_communities
	log("Total communities = " + str(nb_communities), LOG_INFO)
	
	log("Updating referents and communities...")
	crit_referent = math.ceil((nb_members+1) ** (1/STEPMAX))
	outdata_stats["stats"]["crit_referent"] = crit_referent
	nb_referents = 0
	nb_noreferents = 0
	
	community_colors = [lab_gradient(i / (nb_communities-1), 100 if i%2==0 else 50) for i in range(nb_communities)]
	random.shuffle(community_colors)
	
	for community in range(nb_communities):
		for i in communities[community]:
			outdata_wot["nodes"][i]["my_community"] = community
			outdata_wot["nodes"][i]["color"] = "rgba({},{},{},1)".format(*community_colors[community])
			
			if outdata_wot["nodes"][i]["attributes"]["member"] and \
			   outdata_wot["nodes"][i]["attributes"]["inDegree"] >= crit_referent and \
			   outdata_wot["nodes"][i]["attributes"]["outDegree"] >= crit_referent:
				outdata_wot["nodes"][i]["attributes"]["referent"] = True
				nb_referents += 1
			
	nb_noreferents = nb_members - nb_referents
	outdata_stats["stats"]["total_referents"] = nb_referents
	outdata_stats["stats"]["total_noreferents"] = nb_noreferents
	
	if gencoords:
		log("Updating coords...")
		if "--fa2rs" in sys.argv:
			import fa2rs
			layout = fa2rs.Layout.from_graph(graph.edges, len(graph.nodes), fa2rs.Settings(kr=0.02, kg=0, ka=0.01, dissuade_hubs=False, barnes_hut=None, strong_gravity=False, prevent_overlapping=False, jitter_tolerance=0.1))
			for i in range(1000):
				print("{} / 1000".format(i), end="\r")
				layout.iteration()
				#print(layout.get_point(0))
				#print(min(layout.points, key=lambda x:min(x)), max(layout.points, key=lambda x:max(x)), max(layout.points, key=lambda x:math.sqrt(x[0]**2+x[1]**2)))
			i = 0
			for point in layout.points:
				outdata_wot["nodes"][i]["x"] = point[0]
				outdata_wot["nodes"][i]["y"] = point[1]
				i += 1
		else:
			"""
			# this fa2 implementation is too slow
			import forceatlas2 # pip:forceatlas2
			layout = forceatlas2.forceatlas2_networkx_layout(graph, niter=10)
			"""
			
			layout = networkx.layout.fruchterman_reingold_layout(graph)
			i = 0
			for dot in layout:
				outdata_wot["nodes"][i]["x"] = layout[dot][0]
				outdata_wot["nodes"][i]["y"] = layout[dot][1]
				i += 1
	
	log("Computing centralities")
	centralities = {
	#	"c_betweenness": networkx.centrality.betweenness_centrality,
	#	"c_load": networkx.centrality.load_centrality,
	#	"c_eigenvector": networkx.centrality.eigenvector_centrality,
	#	"c_degree": networkx.centrality.degree_centrality,
	#	"c_closeness": networkx.centrality.closeness_centrality,
	#	"c_information": networkx.centrality.information_centrality,
	#	"c_harmonic": networkx.centrality.harmonic_centrality
	}
	centrality_extrema = {}
	for i in centralities:
		try:
			centralities[i] = centralities[i](graph)
		except Exception as e:
			log("Abort computing centrality {}: {}".format(i, e), LOG_ERROR)
			centralities[i] = None
			continue
		centrality_extrema[i] = [centralities[i][0], centralities[i][0]]
	
	i = 0
	for node in outdata_wot["nodes"]:
		attrs = node["attributes"]
		for centrality in centralities:
			if centralities[centrality] == None:
				continue
			attrs[centrality] = centralities[centrality][i]
			if centralities[centrality][i] < centrality_extrema[centrality][0]:
				centrality_extrema[centrality][0] = centralities[centrality][i]
			if centralities[centrality][i] > centrality_extrema[centrality][1]:
				centrality_extrema[centrality][1] = centralities[centrality][i]
		i += 1
	
	outdata_stats["stats"]["centrality_extrema"] = centrality_extrema
	
	log("Saving output...")
	outfile_wot = open(outpath_wot, "w")
	json.dump(outdata_wot, outfile_wot)
	outfile_wot.close()
	outfile_stats = open(outpath_stats, "w")
	json.dump(outdata_stats, outfile_stats)
	outfile_stats.close()
	
	if not "-s" in sys.argv:
		log(subprocess.run(["duniter", "start"], stdout=subprocess.PIPE).stdout.decode("utf-8"))
