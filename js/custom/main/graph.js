function displayGraph(s){
	var filter = new sigma.plugins.filter(s),
		avatarsLoaded = false,
		maxDegree = {
			nbNeighbors: 0,
			inDegree: 0,
			outDegree: 0,
			total: 0
		},
		tabIdentities = [],
		toKeep = s.graph.nodes(),
		communityFiltered = "",
		communitiesHidden = [],
		edgesWereDisplayed = false,
		isDoubleClicked = 0,
		selectedNode = "",
		selectedEdge = "",
		cam = s.camera,
		cesium = 'https://demo.cesium.app' + '/#/app/wot/';

	_.$('loader').style.display = "none";

	// Browse all the nodes to retrieve the maxDegree and to sort the labels for autocompletion search
	s.graph.nodes().forEach(function(node, i, a) {
		var tabnode = [node.label, node.url];
		maxDegree.nbNeighbors = Math.max(maxDegree.nbNeighbors, node.attributes.nbNeighbors);
		maxDegree.inDegree = Math.max(maxDegree.inDegree, node.attributes.inDegree);
		maxDegree.outDegree = Math.max(maxDegree.outDegree, node.attributes.outDegree);
		maxDegree.total = Math.max(maxDegree.total, (node.attributes.outDegree+node.attributes.inDegree));
		tabIdentities.push(tabnode);
	});

	// Launch the forces
	var fa = sigma.layouts.configForceLink(s, {
		outboundAttractionDistribution : true,
		worker: true,
		autoStop: true,
		maxIterations: 250,
		background: true,
		easing: 'cubicInOut',
		scalingRatio: .1
	});

	// Bind the events for displaying the loader during the calculation of nodes position
	fa.bind('start ', function(e) {
		_.$('loader').style.display='block';
		_.$("progress").style.display = "block";
		_.$('loader-title').innerHTML = "Calcul de la position des noeuds...";
		loading(true);
	});
	fa.bind('stop ', function(e) {
		_.$('loader').style.display='none';
		loading(false);
		displayEdges(edgesWereDisplayed);
	});

	// Start the ForceLink algorithm:
	//sigma.layouts.startForceLink();

	// Initialize the controls
	_.$('displayEdges').checked = false;
	_.$('displayAvatars').checked = false;
	_.$('displayNames').checked = false;
	_.$('referentsFilter').checked = false;
	_.$('noreferentsFilter').checked = false;
	_.$('strongGravityMode').checked = false;
	_.$('search').value = '';
	_.$('html5colorpicker').value = '#455660';
	_.$('min-degree').max = maxDegree.nbNeighbors;
	_.$('circleScale').value = 1;
	_.$('max-degree-value').textContent = maxDegree.nbNeighbors;
	for (var i=0; i < _.$('circleSize').length; i++){
		_.$('circleSize')[i].selected = (_.$('circleSize')[i].value=="nbNeighbors");
	}
	
	// Autocompletion
	tabIdentities.sort();
	new autoComplete({
		selector: 'input[id="search"]',
		minChars: 2,
		source: function(term, suggest){
			term = term.toLowerCase();
			var suggestions = [];
			for (i=0;i<tabIdentities.length;i++)
				if (~(tabIdentities[i][0]).toLowerCase().indexOf(term)) suggestions.push(tabIdentities[i]);
			suggest(suggestions);
		},
		renderItem: function (item, search){
			search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
			var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
			return '<div class="autocomplete-suggestion" data-val="'+item[0]+'"><img class="w3-circle img-mini" src="'+item[1]+'"> '+item[0].replace(re, "<b>$1</b>")+'</div>';
		},
		onSelect: function(e, term, item){
			s.graph.nodes().some(function(node){
				var isFound = false;
				if (node.label === term){
					focusNode(cam,s.graph.nodes(node.id));
					isolateNode(s.graph.nodes(node.id));
					_.$('search').value = "";
					isFound = true;
				}
				return isFound;
			});
		}
	});

	// Actions to manipulate the graph
	_.$('min-degree').addEventListener("mousedown", function(){edgesWereDisplayed = s.settings('drawEdges'); displayEdges(false);});
	_.$('min-degree').addEventListener("mouseup", function(){loading(true);setTimeout(function(){displayEdges(edgesWereDisplayed);}, 25);});
	_.$('min-degree').addEventListener("input", applyMinDegreeFilter);  // for Chrome and FF
	_.$('min-degree').addEventListener("change", applyMinDegreeFilter); // for IE10+, that sucks
	_.$('referentsFilter').addEventListener("click", applyCategoryFilter);
	_.$('noreferentsFilter').addEventListener("click", applyCategoryFilter);
	_.$('reset-btn-filters').addEventListener("click", resetFilters);
	_.$('reset-btn-cam').addEventListener("click", centerCamera);
	s.bind('rightClickNode', hideCommunity);
	s.bind('doubleClickNode', filterCommunity);
	s.bind('clickNode', isolateNodeOnClick);
	s.bind('clickEdge', displayEdgeInfos);
	_.$('export').onclick = function() {var output = s.toSVG({download: true, filename: 'wot.svg', size: 1000});};
	_.$('displayEdges').onclick = function() {displayEdges(this.checked);};
	_.$('displayAvatars').onclick = function() {displayAvatars(this.checked);};
	_.$('displayNames').onclick = function() {s.settings('drawLabels',this.checked);s.refresh({skipIndexation: true});};
	_.$('circleSize').onchange = function() {
		var choice = this.value;
		_.$('min-degree').max = maxDegree[choice];
		_.$('min-degree-title').textContent = _.$('circleSize').options[_.$('circleSize').selectedIndex].text;
		_.$('max-degree-value').textContent = maxDegree[choice];
		s.graph.nodes().forEach(function(n) {
			switch (choice) {
				case 'nbNeighbors':
				case 'inDegree':
				case 'outDegree':
					n.size = n.attributes[choice];
				break;
				case 'total':
					n.size = n.attributes.inDegree + n.attributes.outDegree;
				break;
			}
			
		});
		s.refresh({skipIndexation: true});
	};
	_.$('circleScale').oninput = function() {
		_.$('circleScaleValue').textContent = this.value;
		s.settings('minNodeSize',Math.exp(this.value));
		s.settings('maxNodeSize',Math.exp(this.value)*10);
		s.refresh({skipIndexation: true});
	};
	_.$('strongGravityMode').onclick = function () {
		edgesWereDisplayed = s.settings('drawEdges');
		s.settings('animationsTime',500);
		displayEdges(false);
		if (this.checked) {
			sigma.layouts.configForceLink(s, {
				outboundAttractionDistribution : true,
				worker: true,
				autoStop: true,
				maxIterations: 65,
				background: true,
				easing: 'cubicInOut',
				scalingRatio: 1,
				strongGravityMode : true,
				gravity: .5
			});
		} else {
			sigma.layouts.configForceLink(s, {
				outboundAttractionDistribution : true,
				worker: true,
				autoStop: true,
				maxIterations: 76,
				background: true,
				easing: 'cubicInOut',
				scalingRatio: .1
			});
		}
		setTimeout(function(){sigma.layouts.startForceLink();}, 25);
	};
	
	// Keyboard management
	var kbd = sigma.plugins.keyboard(s, s.renderers[0], {zoomingRatio: 1.3});
	// Examples
	// kbd.bind('32', function() {console.log('"Spacebar" key pressed');});
	// kbd.bind('17+65 32+65', function() {console.log('"Ctrl + A"  or "Spacebar + A" pressed');});
	
	// Function to display Edge Infos
	function displayEdgeInfos(e){
		var edge = e.data.edge,
			source = s.graph.nodes(edge.source),
			target = s.graph.nodes(edge.target)
			nodesConcerned = [source.id,target.id];
			
		if(!e.data.captor.isDragging){
			if (selectedEdge === edge){
				edgeAlreadySelected = "";
				resetFilters();
				centerCamera();
				_.$("edgeInfos").style.display = 'none';
				setTimeout(function(){displayEdges(true);}, 1500);
			}else{
				var edgeAlreadySelected = selectedEdge;
				
				if (!edgeAlreadySelected){resetFilters();}
				selectedEdge = edge;
				
				_.$("nodeInfos").style.display = 'none';
				
				_.$("start_cert").textContent = timestampToDate(edge.start_cert);
				_.$("end_cert").textContent = timestampToDate(edge.end_cert);
				
				_.$("avatar_certifieur_name").textContent = source.label;
				_.$("certifieur_link").href = cesium + source.attributes.pubkey + '/' + source.label;
				_.$("avatar_certifieur").style.cursor = "pointer";
				_.$("avatar_certifieur").setAttribute("src", source.url);
				_.$("avatar_certifieur").onclick = function(){history.pushState({ node: source }, source.attributes.pubkey, "./#");isolateNode(source);};
				_.$("avatar_certifieur").setAttribute("title", source.attributes.pubkey);
				
				_.$("avatar_certifie_name").textContent = target.label;
				_.$("certifie_link").href = cesium + target.attributes.pubkey + '/' + target.label;
				_.$("avatar_certifie").style.cursor = "pointer";
				_.$("avatar_certifie").setAttribute("src", target.url);
				_.$("avatar_certifie").onclick = function(){history.pushState({ node: target }, target.attributes.pubkey, "./#");isolateNode(target);};
				_.$("avatar_certifie").setAttribute("title", target.attributes.pubkey);
				
				if (!edgeAlreadySelected){
					var centerx = Math.min(source[cam.readPrefix + 'x'],target[cam.readPrefix + 'x']) + Math.abs((source[cam.readPrefix + 'x']-target[cam.readPrefix + 'x'])/2);
					var centery = Math.min(source[cam.readPrefix + 'y'],target[cam.readPrefix + 'y']) + Math.abs((source[cam.readPrefix + 'y']-target[cam.readPrefix + 'y'])/2);
					
					setTimeout(function(){loading(true);}, 25);
					filter
					  .undo('filterEdge')
					  .nodesBy(function(n) {return isInArray(nodesConcerned, n.id);}, 'filterEdge')
					  .apply();
					  
					displayEdges(false);
					setTimeout(function(){loading(true);}, 25);
					sigma.misc.animation.camera(
						cam,
						{
							x: centerx,
							y: centery,
							ratio: 0.5
						},
						{duration: 1500}
					);
				}
				_.$("edgeInfos").style.display = 'initial';
				if (!edgeAlreadySelected){setTimeout(function(){displayEdges(true);}, 1500);}
			}
		}
	}

	// Functions to display/hide avatars
	function displayAvatarsAfterLoading(bool){
		var type = bool ? 'image' : 'border';

		s.graph.nodes().forEach(function(n) {
			if (!bool || (bool && toKeep[n.id])){n.type = type;}
		});
		s.refresh({skipIndexation: true});
	}

	function displayAvatars(bool){
		var loaded = 0;

		if (!avatarsLoaded){
			var elem = _.$("myBar");

			_.$('myProgress').style.display = bool ? 'initial' : 'none';

			tab_urls.forEach(function(url) {
				sigma.canvas.nodes.image.cache(
					url,
					function() {
						var nbloaded = ++loaded,
							total = tab_urls.length,
							width = Math.round(nbloaded/total * 100);
						
						elem.style.width = width + '%';
						elem.innerHTML = width  + '%';

						if (nbloaded === total){
							avatarsLoaded = true;
							_.$('myProgress').style.display = 'none';
							displayAvatarsAfterLoading(bool);
						}
					}
				);
			});
		}else{
			displayAvatarsAfterLoading(bool);
		}
	}
	
	// Function to display/hide the edges
	function displayEdges(bool){
		loading(true);
		s.settings('drawEdges',bool);
		s.graph.edges().forEach(function(e) {e.hidden = !bool;});
		_.$('displayEdges').checked = bool;
		setTimeout(function(){s.refresh();}, 25);
		setTimeout(function(){loading(false);}, 25);
	}
	
	// Function that moves the camera on the desired node
	function focusNode(camera, node) {
	  sigma.misc.animation.camera(
			camera,
			{
				x: node[camera.readPrefix + 'x'],
				y: node[camera.readPrefix + 'y'],
				ratio: 0.2
			},
			{duration: 1500}
	  );
	}
	
	// Function to center the camera
	function centerCamera(e){
		if(e){edgesWereDisplayed = s.settings('drawEdges');displayEdges(false);}
		sigma.misc.animation.camera(
			cam,
			{x: 0, y: 0, angle: 0, ratio: 1},
			{duration: 1500}
		);
		if(e && edgesWereDisplayed){setTimeout(function(){displayEdges(true);}, 1500);}
	}

	// Function to display/hide a node with his neighboors
	function toggleDisplayNodeNeighboors(bool, node){
		toKeep = bool ? s.graph.neighbors(node.id) : s.graph.nodes();
		toKeep[node.id] = node;
		selectedNode = node;
		
		s.graph.nodes().forEach(function(n) {
			var tmpColor = n.color,
				composantes = tmpColor.substring(5,tmpColor.lastIndexOf(','));

			if (bool){
				if (!toKeep[n.id]){
					n.color = 'rgba(' + composantes + ',0.1)';
					n.borderColor = 'rgba(255,255,255,0.1)';
					if (n.type == 'image'){n.type = 'border';}
				}else{
					n.color = 'rgba(' + composantes + ',1)';
					n.borderColor = 'rgba(0,0,0,1)';
				}
			}else{
				if (selectedNode.type =='image'){n.type='image'}
				n.color = 'rgba(' + composantes + ',1)';
				n.borderColor = 'rgba(255,255,255,1)';
			}
		});
		if (!bool){_.$("nodeInfos").style.display = 'none';selectedNode = "";}

		s.graph.edges().forEach(function(e) {
			var tmpColor = s.graph.nodes(e.source).color,
				composantes = tmpColor.substring(5,tmpColor.lastIndexOf(',')),
				opacity = selectedNode && !((e.source==selectedNode.id) || (e.target==selectedNode.id)) ? '0.1' : '1';
			
			e.color = 'rgba(' + composantes + ',' + opacity + ')';
			e.hidden = !s.settings('drawEdges');
		});
	}
	
	// Functions to isolate a node
	function isolateNodeOnClick(e){
		edgesWereDisplayed = s.settings('drawEdges');
		window.setTimeout(function () {
			if(isDoubleClicked) {
				isDoubleClicked --;
				return
			}

			if(!e.data.captor.isDragging){
				if (selectedNode.id === e.data.node.id){
					history.pushState(null, null, "./#");
					resetFilters();
					toggleDisplayNodeNeighboors(false, selectedNode);
					centerCamera();
					setTimeout(function(){displayEdges(edgesWereDisplayed);}, 1500);
					
				}else{
					history.pushState({ node: e.data.node }, e.data.node.attributes.pubkey, "./#");
					isolateNode(e.data.node);
				}
			}
		},
		sigma.settings.doubleClickTimeout + 100);
	}

	function isolateNode(node){
		resetFilters();
		toggleDisplayNodeNeighboors(true, node);
		focusNode(cam,node);
		_.$("avatar").setAttribute("src", node.url);
		_.$("pseudo").textContent = node.label;
		_.$("referent").textContent = node.attributes.referent ? 'Oui' : 'Non';
		_.$("referent").classList="";
		_.$("referent").classList.add('w3-badge');
		_.$("referent").classList.add(node.attributes.referent ? 'w3-green' : 'w3-red');
		_.$("pubkey").textContent = node.attributes.pubkey;
		_.$("pubkey").href = cesium + node.attributes.pubkey + '/' + node.label;
		_.$("nbVoisins").textContent = node.attributes.nbNeighbors;
		_.$("degree").textContent = s.graph.degree(node.id);
		_.$("indegree").textContent = s.graph.degree(node.id,'in');
		_.$("outdegree").textContent = s.graph.degree(node.id,'out');
		_.$("communityNumber").textContent = node.my_community+1;
		_.$("edgeInfos").style.display = 'none';
		_.$("nodeInfos").style.display = 'initial';
		setTimeout(function(){displayEdges(edgesWereDisplayed);}, 1500);
	}

	// Functions that filters the nodes
	function hideCommunity(e) {
		loading(s.settings('drawEdges'));
		if (communityFiltered == ""){
			communitiesHidden.push(e.data.node.my_community);
			setTimeout(function(){
				filter
				  .undo('hideCommunity')
				  .nodesBy(function(n) {return !isInArray(communitiesHidden, n.my_community);}, 'hideCommunity')
				  .apply();
				displayEdges(s.settings('drawEdges'));
			}, 25);
		}
		setTimeout(function(){loading(false);}, 25);
	}
	
	function filterCommunity(e) {
		isDoubleClicked = 2;
		
		if (communityFiltered == "" || communityFiltered != e.data.node.my_community){
			loading(s.settings('drawEdges'));
			communityFiltered = e.data.node.my_community;
			setTimeout(function(){
				filter
				  .undo()
				  .nodesBy(function(n) {return n.my_community == communityFiltered;}, 'filterCommunity')
				  .apply();
				displayEdges(s.settings('drawEdges'));
			}, 25);
			setTimeout(function(){loading(false);}, 25);
		}else{
			communityFiltered = "";
			resetFilters();
		}
	}
	
	function applyMinDegreeFilter(e) {
		var v = e.target.value;
		
		_.$('min-degree-val').textContent = v;
		filter
		  .undo('min-degree')
		  .nodesBy(function(n) {
			  	switch (_.$('circleSize').value) {
					case 'nbNeighbors':
					case 'inDegree':
					case 'outDegree':
						return n.attributes[_.$('circleSize').value] >= v;
					break;
					case 'total':
						return (n.attributes.inDegree + n.attributes.outDegree) >= v;
					break;
				}
			  
		  }, 'min-degree')
		  .apply();
	}
	
	function applyCategoryFilter(e) {
		var c = (e.originalTarget.id == 'referentsFilter') ? '1' : '0';
		if (c == '1'){_.$('noreferentsFilter').checked = false;}else{_.$('referentsFilter').checked = false;}
		loading(s.settings('drawEdges'));
		setTimeout(function(){
			if (e.target.checked){
				filter
					.undo('node-category')
					.nodesBy(function(n) {return (n.id==selectedNode.id) || (n.attributes.referent == c);}, 'node-category')
					.apply();
			}else{
				filter.undo('node-category').apply();
			}
			if (!s.settings('drawEdges')){displayEdges(false)};
			setTimeout(function(){loading(false);}, 25);
		}, 25);
		
	}

	// Function that resets all filters
	function resetFilters(e){
		edgesWereDisplayed = s.settings('drawEdges');
		communityFiltered = "";
		communitiesHidden = [];
		selectedEdge = "";
		s.settings('drawEdges', false);
		filter.undo().apply();
		displayEdges(false);
		if (selectedNode){toggleDisplayNodeNeighboors(false, selectedNode);}
		_.$('min-degree').value = 0;
		_.$('min-degree-val').textContent = '0';
		_.$('referentsFilter').checked = false;
		_.$('noreferentsFilter').checked = false;
		if (e){displayEdges(edgesWereDisplayed);}
	}
	
	// Function to indicate that we are loading
	function loading(bool){
		var pointer = bool ? "wait" : "pointer",
			move = bool ? "wait" : "move";
		
		document.body.className = move;
		_.$("graph-container").style.cursor = move;
		s.bind('overNode', function(e) {_.$("graph-container").style.cursor = pointer;});
		s.bind('outNode', function(e) {_.$("graph-container").style.cursor = move;});
		s.bind('overEdge', function(e) {_.$("graph-container").style.cursor = pointer;});
		s.bind('outEdge', function(e) {_.$("graph-container").style.cursor = move;});
	}
	
	// Function to manage history
	window.onpopstate = function(event) {
		if (!event.state){
			resetFilters();
			centerCamera();
		}else if (event.state.node){
			isolateNode(event.state.node);
		}else if (event.state.communities){
			console.log(event.state.communities);
		};
	};
}
