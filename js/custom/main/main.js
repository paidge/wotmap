// Disable context menu on right click
if (document.addEventListener) {
	document.addEventListener('contextmenu', function (e) {
		e.preventDefault();
	}, false);
} else {
	document.attachEvent('oncontextmenu', function () {
		window.event.returnValue = false;
	});
}

function loadJSON(file,callback) {   
	var xobj = new XMLHttpRequest();
		xobj.overrideMimeType("application/json");
	xobj.open('GET', file, true);
	xobj.onreadystatechange = function () {
		if (xobj.readyState == 4 && xobj.status == "200") {
			// Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
			callback(xobj.responseText);
		}
	};
	xobj.send(null);  
}

function isInArray(tableau, objet){
 return (tableau.indexOf(objet) != -1);
}

function timestampToDate(timestamp){
	var date = new Date(timestamp*1000);
	 
	jour = date.getDate();
	mois = date.getMonth()+1;
	annee = date.getFullYear();
	
	jour = jour<10 ? '0'+jour : jour;
	mois = mois<10 ? '0'+mois : mois;
	
	return jour+"/"+mois+"/"+annee;
}

// Add a method to the graph model that returns an
// object with every neighbors of a node inside:
sigma.classes.graph.addMethod('neighbors', function(nodeId) {
var k,
	neighbors = {},
	index = this.allNeighborsIndex[nodeId] || {};

for (k in index)
  neighbors[k] = this.nodesIndex[k];

return neighbors;
});

// We store all the img urls in an array to load them before displaying avatars
var tab_urls = [],
	renderer = {
			container: 'graph-container',
			type: 'canvas'
  },
  settings = {
		defaultNodeType: "border",
		borderSize: 1,
		minNodeSize: 2.718,
		maxNodeSize: 27.18,
		enableEdgeHovering: true,
		edgeHoverPrecision: 2,
		edgeHoverSizeRatio: 4,
		edgeHoverExtremities: true,
		edgeColor: "source",
		hideEdgesOnMove:true,
		drawLabels: false,
		drawEdges: false,
		minArrowSize: 8,
		labelThreshold: 17,
		labelSize: "proportional",
		labelSizeRatio: 1.1,
		defaultLabelColor: "rgba(255,255,255,1)",
		animationsTime: 3500,
		zoomingRatio: 1.5,
		doubleClickEnabled: false
  },
  json_url = 'data/wot.json',
  json_stats = 'data/stats.json';

// Load stats
loadJSON(json_stats, function(response) {
	var actual_JSON = JSON.parse(response);
	
	_.$("version").innerHTML = "v.&nbsp;" + actual_JSON.instance.version;
	_.$("crit_referent").textContent = actual_JSON.stats.crit_referent;
	_.$("currency1").textContent = actual_JSON.instance.custom.currency;
	_.$("currency2").textContent = actual_JSON.instance.custom.currency;
	_.$("block").textContent = actual_JSON.instance.custom.block;
	_.$("nb_members").textContent = actual_JSON.stats.total_members;
	_.$("nb_certifs").textContent = actual_JSON.stats.total_edges;
	_.$("nb_communities").textContent = actual_JSON.stats.total_communities;
	_.$("nb_referents").textContent = actual_JSON.stats.total_referents;
	_.$("nb_noreferents").textContent = actual_JSON.stats.total_noreferents;
});

// Load data for the wot
loadJSON(json_url, function(response) {
	var actual_JSON = JSON.parse(response);

	actual_JSON.nodes.forEach(function(n){
		if (!isInArray(tab_urls,n.url)){
			tab_urls.push(n.url);
		}
	});
	
	// Instantiate sigma:
	sigma.parsers.json(
		json_url, 
		{
			renderer: renderer,
			settings: settings
		},
		displayGraph
	);
});

// Menu
_.$('openNav').addEventListener("click", toogle_menu);
function toogle_menu() {
  if (_.$('openNav').classList.contains("is-active")) {
	// On ferme le menu
	_.$("menu").style.left = "-180px";
	_.$("graph-container").style.left = "70px";
	_.$("menu").style.paddingRight = "70px";
	_.$("openNav").classList.remove("is-active");
  } else {
	// On ouvre le menu
	_.$("menu").style.left = "0";
	_.$("graph-container").style.left = "250px";
	_.$("menu").style.paddingRight = "0";
	_.$("openNav").classList.add("is-active");
  }
}
// Accordion
var accordions = document.getElementsByClassName("btn-acc");
for (var i = 0; i < accordions.length; i++) {
    accordions[i].addEventListener('click', function (){
		var is_open = (this.className.indexOf("open") != -1);
		var panel = this.nextElementSibling;
		
		for (var i = 0; i < accordions.length; i++) {
			var btn = accordions[i];
			btn.className = btn.className.replace(" open", "");
			btn.nextElementSibling.style.maxHeight = null;
		}
		
		if (!is_open){
			panel.style.maxHeight = panel.scrollHeight + "px";
			this.classList.add("open");
		}
	});
}